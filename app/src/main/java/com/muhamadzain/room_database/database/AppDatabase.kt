package com.muhamadzain.room_database.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.muhamadzain.room_database.dao.DaoUser
import com.muhamadzain.room_database.dataclass.User

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): DaoUser
}
