package com.muhamadzain.room_database.dataclass

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey val userId: String,
    val name: String,
    val age: Int
)
