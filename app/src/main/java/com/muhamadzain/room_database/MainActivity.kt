package com.muhamadzain.room_database

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.room.Room
import com.muhamadzain.room_database.database.AppDatabase
import com.muhamadzain.room_database.dataclass.User

class MainActivity : AppCompatActivity() {

    private lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // initialize database
        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "UserDB").build()

        // insert users
        val user = User("1", "Muhamad Zainal", 10)
        db.userDao().insertUser(user)
//        // get All users
//        val users = db.userDao().getAllUsers()
//        Log.d("usersData ", users.toString())
//        // get user by id
//        val userById = db.userDao().getUserById("1")
//        Log.d("usersData by Id", userById.toString())
    }
}