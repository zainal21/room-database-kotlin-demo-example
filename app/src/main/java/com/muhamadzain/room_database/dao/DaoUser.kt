package com.muhamadzain.room_database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.muhamadzain.room_database.dataclass.User

@Dao
interface DaoUser {
    @Insert
    fun insertUser(user : User)

    @Query("SELECT * FROM user")
    fun getAllUsers() : List<User>

    @Query("SELECT * FROM user WHERE userId=:userId")
    fun getUserById(userId : String) : User
}